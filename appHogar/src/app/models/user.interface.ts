export interface User {
    uid: string;
    name: string;
    lastName: string;
    email: string;
    username: string;
    telefono: string;
    type: string;
    password: string;
}