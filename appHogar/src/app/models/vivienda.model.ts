interface Marker {
    position: {
      lat: number,
      lng: number,
    };
    title: string;
  }  

export class ViviendaModel {
    uid: string;
    id_vivienda: number;
    tipo_vivienda: string;
	tipo: string;
	valor: string;
	departamento: string;
	ciudad: string;
    direccion: string;
    descripcion: string;
	marker: Marker;
	contacto: {
		id_contacto: string,
		telefono: string,
		email: string,
        nombre: string
    }
    public viviendas?: Array<string>;

    constructor(raw: any) {
        if (raw.hasOwnProperty('viviendas')) {
            this.viviendas = raw.viviendas;
        }
        if (raw.hasOwnProperty('id_vivienda')) {
            this.id_vivienda = raw.id_vivienda;
        }
        if (raw.hasOwnProperty('tipo_vivienda')) {
            this.tipo_vivienda = raw.tipo_vivienda;
        }
        if (raw.hasOwnProperty('tipo')) {
            this.tipo = raw.tipo;
        }
        if (raw.hasOwnProperty('valor')) {
            this.valor = raw.valor;
        }
        if (raw.hasOwnProperty('departamento')) {
            this.departamento = raw.departamento;
        }
        if (raw.hasOwnProperty('ciudad')) {
            this.ciudad = raw.ciudad;
        }
        if (raw.hasOwnProperty('direccion')) {
            this.direccion = raw.direccion;
        }
        if (raw.hasOwnProperty('marker')) {
            this.marker = raw.marker;
        }
        if (raw.hasOwnProperty('contacto')) {
            this.contacto = raw.contacto;
        }
    }
}

export class GetAllViviendas {
    public count: number;
    public vivienda: ViviendaModel[];
    constructor(raw: any) {
        if (raw.hasOwnProperty('count')) {
            this.count = raw.count;
        }
        if (raw.hasOwnProperty('vivienda') && Array.isArray(raw.vivienda)) {
            this.vivienda = raw.vivienda.map((vivienda: any) => {
                return new ViviendaModel(vivienda);
            });
        }
    }

}