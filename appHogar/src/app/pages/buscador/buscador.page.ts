import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.page.html',
  styleUrls: ['./buscador.page.scss'],
})
export class BuscadorPage implements OnInit {

  formBusqueda: FormGroup;
  allViviendas = [];
  viviendas = [];

  constructor(private formBuilder: FormBuilder, private authService: AuthService) { 
    this.formBusqueda = this.createForm();
  }

  private createForm() {
    return this.formBuilder.group({
      tipoBusqueda: ['', Validators.required],
      tipoVivienda: ['', Validators.required],
      ciudad: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.authService.getViviendas().subscribe(report => {
      this.allViviendas = report;
    });
  }

  
  search() {
    let tipoVivienda = this.formBusqueda.controls['tipoVivienda'].value;
    let tipoBusqueda = this.formBusqueda.controls['tipoBusqueda'].value;
    let ciudad = this.formBusqueda.controls['ciudad'].value;
    this.viviendas = [];
    this.allViviendas.forEach(element => {
      if(element.ciudad == ciudad && 
        element.tipo_vivienda == tipoVivienda &&
        element.tipo == tipoBusqueda) {
          this.viviendas.push(element);
      }
    }); 
  }


}
