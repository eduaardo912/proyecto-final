import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmpresaHomePage } from './empresa-home.page';

const routes: Routes = [
  {
    path: '',
    component: EmpresaHomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmpresaHomePageRoutingModule {}
