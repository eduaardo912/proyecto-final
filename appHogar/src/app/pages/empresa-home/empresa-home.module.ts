import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmpresaHomePageRoutingModule } from './empresa-home-routing.module';

import { EmpresaHomePage } from './empresa-home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmpresaHomePageRoutingModule
  ],
  declarations: [EmpresaHomePage]
})
export class EmpresaHomePageModule {}
