import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmpresaHomePage } from './empresa-home.page';

describe('EmpresaHomePage', () => {
  let component: EmpresaHomePage;
  let fixture: ComponentFixture<EmpresaHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaHomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EmpresaHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
