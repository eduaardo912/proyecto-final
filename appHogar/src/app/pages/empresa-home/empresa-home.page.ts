import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-empresa-home',
  templateUrl: './empresa-home.page.html',
  styleUrls: ['./empresa-home.page.scss'],
})
export class EmpresaHomePage implements OnInit {

  user = null;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.user = this.auth.usuario;
  }
  
  signOut(): boolean {
    this.auth.logout();
    this.router.navigate(['../tabs/tab2']);
    return false;
  }

  irRegistrarInmueble(): boolean {
    this.router.navigate(['/registrar']);
    return false;
  }

  irBuscador(): boolean {
    this.router.navigate(['/buscador']);
    return false;
  }

  prueba(): boolean {
    return false;
  }
  
}
