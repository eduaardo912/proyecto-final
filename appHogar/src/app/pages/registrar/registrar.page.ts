import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { ViviendaModel } from 'src/app/models/vivienda.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.page.html',
  styleUrls: ['./registrar.page.scss'],
})
export class RegistrarPage implements OnInit {

  formInmueble: FormGroup;
  inmueble: ViviendaModel;
  user = null;

  constructor(private formBuilder: FormBuilder, private auth: AuthService, private alertController: AlertController) { 
    this.formInmueble = this.createForm();
  }

  ngOnInit() {
    this.user = this.auth.usuario;
  }

  registrarInmueble() {
    this.createInmueble();
    this.auth.saveInmueble(this.inmueble);
    this.showAlert('Información', '', 'Inmbueble registrado correctamente.');
    this.formInmueble.reset();
  }

  private createForm() {
    return this.formBuilder.group({
      tipoInmueble: ['', Validators.required],
      tipo: ['', Validators.required],
      ciudad: ['', Validators.required],
      direccion: ['', Validators.required],
      descripcion: ['', Validators.required],
      title: ['', Validators.required],
      valor: ['', Validators.required]
    });
  }

  private createInmueble() {
    this.inmueble = {
      uid: '',
      id_vivienda: 0,
      tipo: this.formInmueble.controls['tipo'].value,
      tipo_vivienda:  this.formInmueble.controls['tipoInmueble'].value,
      valor:  this.formInmueble.controls['valor'].value,
      departamento: '', 
      ciudad: this.formInmueble.controls['ciudad'].value,
      direccion: this.formInmueble.controls['direccion'].value, 
      descripcion: this.formInmueble.controls['descripcion'].value, 
      marker: {
        position: {
          lat: 0,
          lng: 0
        },
        title: this.formInmueble.controls['title'].value
      }, 
      contacto: {
        id_contacto: this.user.uid,
        telefono: this.user.telefono,
        email: this.user.email,
        nombre: this.user.name + ' ' + this.user.lastName
      }
    }
    console.log(this.inmueble);
  }

  showAlert(header: string, subHeader: string, msg: string) {
    this.alertController.create({
      header : header,
      subHeader : subHeader,
      message : msg,
      buttons : ['Aceptar']
      })
    .then(alert => alert.present());
  }

}
