import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

export interface ParametrosBusqueda {
  tipoVivienda: string,
  tipoBusqueda: string,
  ciudad: string
}

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.page.html',
  styleUrls: ['./resultados.page.scss'],
})
export class ResultadosPage implements OnInit {

  allViviendas = [];
  viviendas = [];

  parametrosBusqueda: {
    tipoVivienda: string,
    tipoBusqueda: string,
    ciudad: string
  };

  constructor(private activateRoute: ActivatedRoute, private authService: AuthService) { }

  ngOnInit() {
    this.parametrosBusqueda = {
      tipoVivienda: this.activateRoute.snapshot.params.tipoVivienda,
      tipoBusqueda: this.activateRoute.snapshot.params.tipoBusqueda,
      ciudad: this.activateRoute.snapshot.params.ciudad
    };
    this.authService.getViviendas().subscribe(report => {
      this.allViviendas = report;
      this.allViviendas.forEach(element => {
        if(element.ciudad == this.parametrosBusqueda.ciudad && 
          element.tipo_vivienda == this.parametrosBusqueda.tipoVivienda &&
          element.tipo == this.parametrosBusqueda.tipoBusqueda) {
            this.viviendas.push(element);
        }
      });
    });
  }
  
}
