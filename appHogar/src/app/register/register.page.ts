import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';

import { User } from '../models/user.interface';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  formRegister: FormGroup;
  private user: User;

  constructor(private formBuilder: FormBuilder, 
      public navCtrl: NavController, 
      private authService: AuthService,
      private alertController: AlertController,
      private router: Router) {

      this.formRegister = this.createForm();
  }

  ngOnInit() {

  }

  private createForm() {
    return this.formBuilder.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      tel: ['', Validators.required],
      username: ['', Validators.required],
      typeUser: ['', Validators.required],
      password: ['', Validators.required],
      confirm: ['', Validators.required]
    });
  }

  registrar() {
    if (this.confirmPassword(this.formRegister.controls['password'].value, this.formRegister.controls['confirm'].value)) {    
      if (this.confirmEmail(this.formRegister.controls['email'].value) && this.confirmUsername(this.formRegister.controls['username'].value)) {
        this.createUser();
        this.authService.saveNewUser(this.user, this.formRegister.controls['password'].value);
        this.showAlert('Bienvenido ' + this.formRegister.controls['name'].value, '', 'Usuario creado correctamente.');
        this.formRegister.reset();
        this.router.navigate(['/tabs/tab2']);
      }
    }
  }

  private confirmPassword(password: String, confirm: String): boolean {
    if (password === confirm) {
      return true;
    } else {
      this.showAlert('Contraseña incorrecta', '', 'Las contraseñas no coinciden.');
      return false;
    }
  }

  private confirmEmail(email: string): boolean {
    return true;
  }

  private confirmUsername(username: string): boolean {
    return true;
  }

  private createUser() {
    this.user = {
      uid: '0',
      name: this.formRegister.controls['name'].value,
      lastName: this.formRegister.controls['lastName'].value,
      email: this.formRegister.controls['email'].value,
      username: this.formRegister.controls['username'].value,
      telefono: this.formRegister.controls['tel'].value,
      type: this.formRegister.controls['typeUser'].value,
      password: ''
    }
  }

  showAlert(header: string, subHeader: string, msg: string) {
    this.alertController.create({
      header : header,
      subHeader : subHeader,
      message : msg,
      buttons : ['Ok']
      })
    .then(alert => alert.present());
  }

}
