import { Injectable, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AngularFireDatabase } from '@angular/fire/database';
import { User } from '../models/user.interface';

import * as CryptoJS from 'crypto-js';
import { ViviendaModel } from '../models/vivienda.model';

export interface UserG {
  uid: string;
  email: string;
  displayName: string;
  emailVerified: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit {
  
  private SECERET_KEY: string = 'Secret_Key';
  public user$: Observable<UserG>;
  public usuario: User;
  public allUsers = [];

  constructor(private afAuth:AngularFireAuth, private afs:AngularFirestore, 
    private afDB:AngularFireDatabase) {
    this.user$ = this.afAuth.authState.pipe(
      switchMap((user) => {
        if (user) {
          return this.afs.doc<UserG>(`users/${user.uid}`).valueChanges();  // Recuperar los datos si el usuario ya existe
        }
        return of(null);
      })
    );
    this.getUsers().subscribe(data => {
      this.allUsers = data;
    });
  }

  ngOnInit() {

  }

  async login(email: string, password: string){
    try {
      this.allUsers.forEach(element => {
        if (element.email === email || element.username === email) {
          if (this.getPasswordEcrypted(element.password) === password) {
            this.usuario = element;
            //this.setUser(element);
          }
        }
      });
    } catch (error) {
      console.log('Error login:', error);
    }
  }

  async loginGoogle(): Promise<UserG> {
    try {
      const { user } = await this.afAuth.signInWithPopup(new auth.GoogleAuthProvider());
      this.updateUserData(user);
      return user;
    } catch (error) {
      console.log('Error login google:', error);
    }
  }

  async register(email:string, password:string): Promise<UserG> {
    try {
      const { user } = await this.afAuth.createUserWithEmailAndPassword(email, password);
      await this.sendVerificationEmail();
      return user;
    } catch (error) {
      console.log('Error login:', error);
    }
  }

  async sendVerificationEmail(): Promise<void> {
    try {
      return (await this.afAuth.currentUser).sendEmailVerification();
    } catch (error) {
      console.log('Error login:', error);
    }
  }

  isEmailVerified(user: UserG): boolean {
    return user.emailVerified === true ? true : false;
  }
  
  async logout(): Promise<void> {
    try {
      await this.afAuth.signOut();
      
    } catch (error) {
      console.log('Error logout:', error);
    }
    this.usuario = null;
  }

  async resetPassword(email:string): Promise<void> {
    try {
      return this.afAuth.sendPasswordResetEmail(email);
    } catch (error) {
      console.log('Error logout:', error);
    }
  }

  private updateUserData(user: UserG) {
    const userRef:AngularFirestoreDocument<UserG> = this.afs.doc(`users/${user.uid}`);
    const data: UserG = {
      uid: user.uid,  
      emailVerified: user.emailVerified,
      displayName: user.displayName,
      email: user.email,
    };
    this.usuario = {
      uid: user.uid,
      name: user.displayName,
      lastName: '',
      email: user.email,
      username: user.email,
      telefono: '',
      type: 'usuario',
      password: ''
    }
    return userRef.set(data, { merge: true });
  }

  public saveNewUser(user: User, password: string) {
    user.password = this.setPasswordEncrypted(password);
    let key = this.afDB.list('/usuarios/').push(user).key;
    user.uid = key;
    this.afDB.database.ref('usuarios/'+user.uid).set(user);
    this.allUsers.push(user);
  }

  public saveInmueble(inmueble: ViviendaModel) {
    let key = this.afDB.list('/viviendas/').push(inmueble).key;
    inmueble.uid = key;
    this.afDB.database.ref('viviendas/'+inmueble.uid).set(inmueble);
  }

  getUserAuth() {
    return this.afAuth.authState;
  }

  getUsers() {
    return this.afDB.list('usuarios/').valueChanges();
  }

  public getViviendas() {
    return this.afDB.list('viviendas/').valueChanges();
  }

  private setPasswordEncrypted(contra: string): string {
    return CryptoJS.AES.encrypt(contra.trim(), this.SECERET_KEY.trim()).toString();
  }

  public getPasswordEcrypted(passwordEnc: string): string {
    return CryptoJS.AES.decrypt(passwordEnc.trim(), this.SECERET_KEY.trim()).toString(CryptoJS.enc.Utf8);
  }

}