import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetAllViviendas, ViviendaModel } from '../models/vivienda.model';

@Injectable({
  providedIn: 'root'
})
export class ViviendasService {

  public data: Array<ViviendaModel>;
  public url = 'http://localhost:3000/viviendas';
  public http: HttpClient;

  constructor(http: HttpClient) { 
    this.http = http;
  }

  public getViviendas(): Observable<GetAllViviendas> {
    return this.http.get<GetAllViviendas>(this.url);
  }
}
