import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

export interface ParametrosBusqueda {
  tipoVivienda: string,
  tipoBusqueda: string,
  ciudad: string
}

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  formBusqueda: FormGroup;
  busqueda: ParametrosBusqueda;

  constructor(private formBuilder: FormBuilder, 
    private router: Router) {
    this.formBusqueda = this.createForm();
  }

  private createForm() {
    return this.formBuilder.group({
      tipoBusqueda: ['', Validators.required],
      tipoVivienda: ['', Validators.required],
      ciudad: ['', Validators.required],
    });
  }

  search() {
    this.busqueda = {
      tipoVivienda : this.formBusqueda.controls['tipoVivienda'].value,
      tipoBusqueda : this.formBusqueda.controls['tipoBusqueda'].value,
      ciudad : this.formBusqueda.controls['ciudad'].value
    }
    this.formBusqueda.reset();
    this.router.navigate([
        '/resultados', 
        this.busqueda.tipoVivienda, 
        this.busqueda.tipoBusqueda,
        this.busqueda.ciudad
      ]);
  }

}
