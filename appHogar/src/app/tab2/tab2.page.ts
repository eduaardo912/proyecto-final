import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { User } from '../models/user.interface';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  private ROLE_EMPRESA: string = 'inmobiliaria';
  private ROLE_VENDEDOR: string = 'vendedor';
  private ROLE_ARRENDADOR: string = 'arrendador';
  private ROLE_USUARIO: string = 'usuario';
  
  formLogin: FormGroup;
  private user: User;

  constructor(private authService: AuthService, private router: Router, 
    private formBuilder: FormBuilder, private alertController: AlertController) 
  {
      this.formLogin = this.createForm();
  }

  private createForm() {
    return this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  
  async onLogin() {
    try {
      await this.authService.login(
        this.formLogin.controls['email'].value, 
        this.formLogin.controls['password'].value
      );
      if (this.authService.usuario != null) {
        this.user = this.authService.usuario;
        this.formLogin.reset();
        this.redirectUser();
      } else {
        this.showAlert('Error en inicio de sesión', '', 'El nombre de usuario o contraseña son incorrectos. Inténtelo de nuevo.')
        this.formLogin.controls['password'].reset();
      }
    } catch (error) {
      console.log('Error onLogin:', error);
    }
  }

  async onLoginGoogle() {
    try {
      const user = await this.authService.loginGoogle();
      if (user && this.authService.usuario) {
        this.user = this.authService.usuario;
        this.redirectUser();
      }
    } catch (error) {
      console.log('Error:', error);
    }
  }

  private redirectUser() {
    let role = this.user.type;
    if (role == this.ROLE_EMPRESA) {
      this.router.navigate(['/empresa-home']);
    }
    if (role == this.ROLE_VENDEDOR) {
      this.router.navigate(['/user-home']);
    }
    if (role == this.ROLE_ARRENDADOR) {
      this.router.navigate(['/user-home']);
    }
    if (role == this.ROLE_USUARIO) {
      this.router.navigate(['/home']);
    }
  }

  showAlert(header: string, subHeader: string, msg: string) {
    this.alertController.create({
      header : header,
      subHeader : subHeader,
      message : msg,
      buttons : ['Ok']
      })
    .then(alert => alert.present());
  }

}
