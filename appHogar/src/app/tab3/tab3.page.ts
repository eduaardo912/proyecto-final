import { Component, OnInit } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { LoadingController } from '@ionic/angular';
import { delay } from 'rxjs/operators';
import { ViviendaModel } from '../models/vivienda.model';
import { ViviendasService } from '../services/viviendas.service';

declare var google;

interface Marker {
  position: {
    lat: number,
    lng: number,
  };
  title: string;
}

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  public data: Array<ViviendaModel> = null;
  private map = null;
  private posicion: Marker;
  public latitud: any;
  public longitud: any; 

  constructor(private viviendaService: ViviendasService, public geolocation: Geolocation, 
    public loadingCtrl: LoadingController) {

  }

  ngOnInit(): void {
    this.presentLoading();
    this.getGeolocation();
  }

  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Espera un momento...',
      duration: 5000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    this.getGeolocation();
    this.indexViviendas();
    this.loadMap();
  }

  async getGeolocation() {
    await this.geolocation.getCurrentPosition().then((geoposition: Geoposition) => {
      this.posicion = {
        position: {
          lat: geoposition.coords.latitude,
          lng: geoposition.coords.longitude
        },
        title: 'Yo'
      }
      const image =
          "https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-13/48/User-Green-icon.png";
      this.addMarker(this.posicion, image, 1);
    });
  }

  indexViviendas() {  
    this.viviendaService.getViviendas().subscribe(result => {
      this.data = result.vivienda;
      if (this.data) {
        this.data.forEach(element => {
          this.addMarker(element.marker, "https://icons.iconarchive.com/icons/emey87/trainee/48/Home-icon.png", 0);
        });
      }
    });
  }

  async loadMap() {
    // create a new map by passing HTMLElement
    const mapEle: HTMLElement = await document.getElementById('map');
    // create LatLng object
    const myLatLng = await {lat: this.latitud, lng: this.longitud};
    // create map
    this.map = await new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 16
    });
  
    await google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
    });
  }

  addMarker(marker: Marker, image: string, opcion: number) {
    if (opcion == 1) {
      this.latitud = marker.position.lat;
      this.longitud = marker.position.lng;
    }
    return new google.maps.Marker({
      position: marker.position,
      map: this.map,
      title: marker.title,
      icon: image,
      animation: google.maps.Animation.DROP, 
    });
  }

}
