// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC8TjVNsPR8LxmBNF2CxMj7KsAN5cMUkfg",
    authDomain: "apphogar-29931.firebaseapp.com",
    databaseURL: "https://apphogar-29931.firebaseio.com",
    projectId: "apphogar-29931",
    storageBucket: "apphogar-29931.appspot.com",
    messagingSenderId: "877436416723",
    appId: "1:877436416723:web:ecbec7b93f92269c575a26"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
